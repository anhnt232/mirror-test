using System.Collections.Generic;
using UnityEngine;
using Mirror;


public class JammoHealth : NetworkBehaviour
{
    [Header("Settings")]
    [SerializeField] private int maxHealth = 100;
    [SerializeField] private int damagePerPress = 10;

    [SyncVar]
    private int currentHealth;

    public delegate void HealthChangeDelegate(int currentHealth, int maxHealth);

    public event HealthChangeDelegate onHealthChanged;

    [Server]
    private void SetHealth(int value)
    {
        currentHealth = value;
        //Debug.Log("On the Server: Current health: " + currentHealth);
        // harm stuff here

        // invoke on server
        this.onHealthChanged?.Invoke(currentHealth, maxHealth);
        // invoke on client
        RpcOnHealthChanged(currentHealth, maxHealth);
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        SetHealth(maxHealth);
    }

    [Command]
    public void CmdDealDamage()
    {
        SetHealth(Mathf.Max(currentHealth - damagePerPress, 0));
    }


    [ClientRpc]
    private void RpcOnHealthChanged(int currentHealth, int maxHealth)
    {
        this.onHealthChanged?.Invoke(currentHealth, maxHealth);
        //Debug.Log("On the Client: Current health: " + currentHealth);
    }

    [ClientCallback]
    private void Update()
    {
        if (!hasAuthority) { return; }

        if (Input.GetKeyDown(KeyCode.X))
        {
            CmdDealDamage();
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class TestNetworkManager : NetworkManager
{
    public override void OnStartServer()
    {
        Debug.Log("Server start");
        base.OnStartServer();
    }

    public override void OnStopServer()
    {
        Debug.Log("Server stoped");
        base.OnStopServer();
    }

    public override void OnClientConnect()
    {
        Debug.Log("Client connected");
        base.OnClientConnect();
    }

    public override void OnClientDisconnect()
    {
        Debug.Log("Client disconnected");
        base.OnClientDisconnect();
    }
}

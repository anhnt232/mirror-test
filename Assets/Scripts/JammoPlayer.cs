using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using TMPro;

public class JammoPlayer : NetworkBehaviour
{
    private JammoCharacterSkinController skinController;
    private CinemachineFreeLook freeLookCam;
    private string username;
    private string color;
    private TextMeshProUGUI playerNameText;

    [SerializeField] private JammoHealth health = null;
    [SerializeField] private Image healthBarImage = null;

    [SyncVar(hook = nameof(DisplayPlayerName))]
    public string playerDisplayName;

    [SyncVar(hook = nameof(DisplayPlayerColor))]
    public string playerColor;

    private void Awake()
    {
        skinController = GetComponent<JammoCharacterSkinController>();

        playerNameText = transform.Find("Canvas").Find("Name").GetComponent<TextMeshProUGUI>();

        healthBarImage = transform.Find("Canvas").Find("BarHealth").GetComponent<Image>();

        health = GetComponent<JammoHealth>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start JammoPlayer " + this.name);
        if (!hasAuthority)
        {
            return;
        }

    }

    private void Update()
    {
        if (!hasAuthority) { return; }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            CmdSendName(this.username, this.color);
        }
    }


    [Command]
    public void CmdSendName(string playerName, string color)
    {
        playerDisplayName = playerName;
        playerColor = color;

        // no rpc needed because the magic of SyncVar.
        // a syncvar listen to data that is on the server
        // thus, we change data on the server and ALL clients update the name with the use of the hook.
    }


    public void DisplayPlayerName(string oldName, string newName)
    {
        //Debug.Log($"Player {this.name} changed name from " + oldName + " to " + newName);
        SetName(newName);
    }

    public void DisplayPlayerColor(string oldColor, string newColor)
    {
        SetColor(newColor);
    }

    public override void OnStartLocalPlayer()
    {
        freeLookCam = GameObject.FindObjectOfType<CinemachineFreeLook>();
        freeLookCam.Follow = this.transform;
        freeLookCam.LookAt = this.transform;

        this.username = PlayerPrefs.GetString("username", "kaka");
        this.color = PlayerPrefs.GetString("color", "");

        SetName(this.username);
        SetColor(this.color);

        Debug.Log(this.name.ToString() + " called: " + this.username + " is local Player");

        CmdSendName(this.username, this.color);
    }


    public void SetName(string username)
    {
        playerNameText.SetText(username);
    }
    public string GetName()
    {
        return playerNameText.text;
    }

    public void SetColor(string color)
    {
        if (color == "red")
        {
            skinController.ChangeMaterialSettings(0);
            playerNameText.color = Color.red;
        }
        if (color == "black")
        {
            skinController.ChangeMaterialSettings(1);
            playerNameText.color = Color.black;
        }
        if (color == "blue")
        {
            skinController.ChangeMaterialSettings(2);
            playerNameText.color = Color.blue;
        }
        if (color == "yellow")
        {
            skinController.ChangeMaterialSettings(3);
            playerNameText.color = Color.yellow;
        }
    }
    public int GetColor()
    {
        return skinController.colorIndex;
    }


    private void OnEnable()
    {
        health.onHealthChanged += HandleHealthChanged;
    }

    private void OnDisable()
    {
        health.onHealthChanged -= HandleHealthChanged;
    }

    void HandleHealthChanged(int currentHealth, int maxHealth)
    {
        healthBarImage.fillAmount = (float)currentHealth / maxHealth;
    }

}

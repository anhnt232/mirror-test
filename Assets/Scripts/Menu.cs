﻿
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class RoomEvent : UnityEvent<string> { };
public class Menu : MonoBehaviour
{

    public RoomEvent onPlayGame = new RoomEvent();
    public RoomEvent onChangeUsername = new RoomEvent();
    public RoomEvent onChangeColor = new RoomEvent();

    public Button playButton;
    public Button redButton;
    public Button blackButton;
    public Button yellowButton;
    public Button blueButton;
    public TMP_InputField userNameInput;
    public TMP_Text gameTitle;
    

    private string[] usernames = { "Coco", "Maya", "Koda", "Dexter", "Benji" };

    public void Init()
    {
        string userName = PlayerPrefs.GetString("username");
        string color = PlayerPrefs.GetString("color");
        if (userName.Length > 0)
        {
            userNameInput.text = userName;
        }
        else
        {
            userNameInput.text = usernames[Random.Range(0, usernames.Length)];
        }
        redButton.onClick.AddListener(() => onChangeColor.Invoke("red"));
        blackButton.onClick.AddListener(() => onChangeColor.Invoke("black"));
        blueButton.onClick.AddListener(() => onChangeColor.Invoke("blue"));
        yellowButton.onClick.AddListener(() => onChangeColor.Invoke("yellow"));
        if (color == "red")
        {
            redButton.Select();
            redButton.onClick.Invoke();
        }
        if (color == "black")
        {
            blackButton.Select();
            blackButton.onClick.Invoke();
        }
        if (color == "blue")
        {
            blueButton.Select();
            blueButton.onClick.Invoke();
        }
        if (color == "yellow")
        {
            yellowButton.Select();
            yellowButton.onClick.Invoke();
        }
        userNameInput.onEndEdit.AddListener((string text) => { onChangeUsername.Invoke(userNameInput.text); });

        userNameInput.onEndEdit.Invoke(userNameInput.text);

        playButton.onClick.AddListener(() => onPlayGame.Invoke(userNameInput.text));
    }

    
}

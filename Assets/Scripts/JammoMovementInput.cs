
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

//This script requires you to have setup your animator with 3 parameters, "InputMagnitude", "InputX", "InputZ"
//With a blend tree to control the inputmagnitude and allow blending between animations.
[RequireComponent(typeof(CharacterController))]
public class JammoMovementInput : NetworkBehaviour
{

    public float Velocity;
    [Space]

	public float InputX;
	public float InputZ;
	public Vector3 desiredMoveDirection;
	public bool blockRotationPlayer;
	public float desiredRotationSpeed = 0.1f;
    public Animator animator;
    public float Speed;
	public float allowPlayerRotation = 0.1f;
	public Camera cam;
	public CharacterController controller;
	public bool isGrounded;

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float HorizontalAnimSmoothTime = 0.2f;
    [Range(0, 1f)]
    public float VerticalAnimTime = 0.2f;
    [Range(0,1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    public float verticalVel;
    private Vector3 moveVector;

    private SoundsPlayer soundsPlayer = null;
    protected Joystick joystick;

    private void Awake()
    {
    }

    // Use this for initialization
    void Start () {
        Debug.Log("Start Movement Input " + this.name);
        if (isLocalPlayer)
        {
            animator = this.GetComponent<Animator>();
            cam = Camera.main;
            controller = this.GetComponent<CharacterController>();
            joystick = GameObject.FindWithTag("joystick").GetComponent<Joystick>();

            soundsPlayer = transform.Find("Sounds").GetComponent<SoundsPlayer>();
           
            //Debug.Log(soundsPlayer);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (isLocalPlayer)
        {
            InputMagnitude();

            isGrounded = controller.isGrounded;
            if (isGrounded)
            {
                verticalVel -= 0;
            }
            else
            {
                verticalVel -= 1;
            }
            moveVector = new Vector3(0, verticalVel * .2f * Time.deltaTime, 0);
            controller.Move(moveVector);
        }

    }

	void InputMagnitude() {
        //Calculate Input Vectors
        InputX = (joystick.Horizontal + Input.GetAxis("Horizontal")) > 0 ? 1 : ((joystick.Horizontal + Input.GetAxis("Horizontal")) < 0 ? -1 : 0);
        InputX *= 0.5f;

        InputZ = (joystick.Vertical + Input.GetAxis("Vertical")) > 0 ? 1 : ((joystick.Vertical + Input.GetAxis("Vertical")) < 0 ? -1 : 0);
        InputZ *= 0.5f;

        //Calculate the Input Magnitude
        Speed = new Vector2(InputX, InputZ).sqrMagnitude;

        //Physically move player

		if (Speed > allowPlayerRotation) {
			animator.SetFloat ("Blend", Speed, StartAnimTime, Time.deltaTime);
			PlayerMoveAndRotation ();
            if (soundsPlayer != null)
            {
                if (!soundsPlayer.IsPlaying(0))
                {
                    soundsPlayer.Play(0);
                }
            }
        } else if (Speed < allowPlayerRotation) {
			animator.SetFloat ("Blend", Speed, StopAnimTime, Time.deltaTime);
            if(soundsPlayer != null)
            {
                soundsPlayer.Stop(0);
            }
        }
    }

    void PlayerMoveAndRotation()
    {
        var forward = cam.transform.forward;
        var right = cam.transform.right;

        forward.y = 0f;
        right.y = 0f;

        forward.Normalize();
        right.Normalize();

        desiredMoveDirection = forward * InputZ + right * InputX;

        if (blockRotationPlayer == false)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
            controller.Move(desiredMoveDirection * Time.deltaTime * Velocity);
        }
    }
}

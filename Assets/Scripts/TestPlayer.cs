using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class TestPlayer : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnHolaCountChanged))]
    public int holaCount = 0;

    void HandleMovement()
    {
        if (isLocalPlayer)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(moveHorizontal*0.1f, moveVertical*0.1f, 0);

            transform.position = transform.position + movement;
        }
    }

    void Update()
    {
        HandleMovement();

        if(isLocalPlayer && Input.GetKeyDown(KeyCode.X))
        {
            Debug.Log("Sending Hola to the server");

            Hola();
        }

        
    }
    public override void OnStartServer()
    {
        Debug.Log("Player has been spawned on the server!");
        base.OnStartServer();
    }

    [Command]
    void Hola()
    {
        Debug.Log("Received Hola from Client!");
        holaCount += 1;
        ReplyHola();
    }

    [TargetRpc]
    void ReplyHola()
    {
        Debug.Log("Received Hola from server");
    }

    [ClientRpc]
    void TooHight()
    {
        Debug.Log("Too hight");
    }

    void OnHolaCountChanged(int oldCount, int newCount)
    {
        Debug.Log($"Old count {oldCount}, new count {newCount}");
    }
}

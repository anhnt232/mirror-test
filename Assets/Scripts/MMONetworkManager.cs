using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


public struct CreateMMOCharacterMessage : NetworkMessage
{
    public string name;
    public string color;
};

public class MMONetworkManager : NetworkManager
{
    public override void OnStartServer()
    {
        base.OnStartServer();

        NetworkServer.RegisterHandler<CreateMMOCharacterMessage> (OnCreateCharacter);
    }

    public override void OnClientConnect()
    {
        base.OnClientConnect();

        // you can send the message here, or wherever else you want
        CreateMMOCharacterMessage characterMessage = new()
        {
            name = PlayerPrefs.GetString("username", "kaka"),
            color = PlayerPrefs.GetString("color", "")
        };
        Debug.Log("OnClientConnect send Create Char");
        NetworkClient.Send(characterMessage);

    }

    void OnCreateCharacter(NetworkConnectionToClient conn, CreateMMOCharacterMessage message)
    {
        GameObject gameobject = Instantiate(playerPrefab);

        JammoPlayer player = gameobject.GetComponent<JammoPlayer>();

        //player.name = $"Jammo_{message.name}_C{message.color}_{conn}";
        //Debug.Log(player.name + " is Created");
        player.SetName(message.name);
        player.SetColor(message.color);

        NetworkServer.AddPlayerForConnection(conn, gameobject);

    }
    
}

